NAME = wolf3d

SRC = main.c\
	draw.c\
	env.c\
	mlx.c\
	map.c\
	key_hook.c\
	mouse_hook.c\
	exit.c\
	color_by_pixel_in_map.c\
	color_by_pixel_in_game.c\
	player.c\
	intersection.c\
	is_near_int.c\
	colors_on_walls.c\

UNAME := $(shell uname)

ifeq ($(UNAME), Linux)

MLX_FLAG = -L/usr/X11/lib -lmlx -lXext -lX11 -lm

endif

ifeq ($(UNAME), Darwin)

MLX_FLAG = -lmlx -framework OpenGL -framework AppKit

endif

LIB = -L ./libft/ -lft -lm

SRC_O = $(SRC:.c=.o)

.c.o:
	gcc $(FLAG) -c $<

INC = include.h

INC_LIB = libft/libft.h

FLAG = -Wall -Werror -Wextra

all: $(NAME)

$(NAME): $(SRC_O) $(INC_LIB) $(INC)
	make -C ./libft/
	gcc -o $(NAME) $(SRC_O) $(LIB) $(MLX_FLAG)


clean:
	rm -f $(SRC_O)
	@make clean -C ./libft/

fclean: clean
	rm -f $(NAME)
	make fclean -C ./libft/

re: fclean all

re-without-lib:
	rm -f $(SRC_O)
	rm -f $(NAME)
	make

test: re-without-lib
	./$(NAME) maps/labirinth.txt