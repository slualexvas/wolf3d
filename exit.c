/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:24 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:04:23 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

void	print_error_and_exit(char *msg, t_env *env)
{
	ft_putstr("\033[31m");
	ft_putendl(msg);
	ft_putstr("\033[0m");
	env_destroy(env);
	exit(EXIT_SUCCESS);
}
