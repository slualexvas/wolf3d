/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rgb.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 14:47:11 by oslutsky          #+#    #+#             */
/*   Updated: 2017/12/02 18:13:46 by ykondrat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rgb.h"

int			rgb_to_int(t_rgb rgb)
{
	return (rgb.r * 0x10000 + rgb.g * 0x100 + rgb.b);
}

t_rgb		int_to_rgb(int color)
{
	t_rgb	rgb;

	rgb.b = color % 0x100;
	color /= 0x100;
	rgb.g = color % 0x100;
	color /= 0x100;
	rgb.r = color % 0x100;
	return (rgb);
}

t_rgb		weighted_color(t_rgb color1, t_rgb color2, float w1, float w2)
{
	t_rgb	color3;

	if (w1 <= 0)
		return (color2);
	if (w2 <= 0)
		return (color1);
	w1 = w1 / (w1 + w2);
	w2 = 1 - w1;
	color3.r = color1.r * w1 + color2.r * w2;
	color3.g = color1.g * w1 + color2.g * w2;
	color3.b = color1.b * w1 + color2.b * w2;
	return (color3);
}

t_rgb		ft_rgb(unsigned char r, unsigned char g, unsigned char b)
{
	t_rgb	res;

	res.r = r;
	res.g = g;
	res.b = b;
	return (res);
}

/*
**	Якщо light < 1, то вважається, що світла замало,
**	і колір змішується з чорним;
**	Якщо light > 1, то вважається, що світла забагато,
**	і колір змішується з білим.
*/

t_rgb		color_by_light(t_rgb color, double light)
{
	if (light <= 0)
		return (ft_rgb(0, 0, 0));
	else if (light <= 1)
		return (weighted_color(color, ft_rgb(0, 0, 0), light, 1 - light));
	else
		return (weighted_color(color, ft_rgb(255, 255, 255),
					1 / light, 1 - 1 / light));
}
