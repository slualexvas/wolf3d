/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/16 13:37:19 by oslutsky          #+#    #+#             */
/*   Updated: 2017/09/16 13:44:32 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# define PRINTF_LEN_L	1
# define PRINTF_LEN_H	2
# define PRINTF_LEN_J	3
# define PRINTF_LEN_Z	4
# define PRINTF_LEN_LL	5
# define PRINTF_LEN_HH	6

typedef struct	s_printf_conversion_vars
{
	int			reshetka;
	int			zero;
	int			minus;
	int			plus;
	int			space;
	int			width;
	char		conv_char;
	int			conv_len;
	int			len_modifier;
	int			precision;
	char		sign;
	int			was_p;
	int			conv_printed;
}				t_pcv;

typedef struct	s_printf_arg_vars
{
	char		**pstr;
	const char	*fmt;
	va_list		args;
	size_t		i;
	size_t		fmt_len;
	int			res;
	t_pcv		pcv;
}				t_pav;

int				ft_printf(const char *fmt, ...);
int				ft_sprintf(char **pstr, const char *fmt, ...);
int				ft_vsprintf(char **pstr, const char *fmt, va_list args);

void			parse_arg(t_pav *pav);
void			init_pav(t_pav *pav, char **pstr, const char *fmt,
					va_list args);
void			init_pcv(t_pcv *pcv);
int				ft_printf_add_str_to_pav(t_pav *pav, const char *s,
					size_t s_len);
int				ft_printf_add_str_using_width(t_pav *pav, char *s,
					size_t s_len);

void			ft_printf_c(t_pav *pav);
void			ft_printf_s(t_pav *pav);
void			ft_printf_di(t_pav *pav);
void			ft_printf_boux(t_pav *pav);
void			ft_printf_p(t_pav *pav);
void			ft_printf_uppercase_doucs(t_pav *pav);
void			ft_printf_f(t_pav *pav);
void			ft_printf_percent(t_pav *pav);
void			ft_printf_v(t_pav *pav);
void			ft_printf_default(t_pav *pav);

void			ft_wctomb(wchar_t c, char *s, int *s_len);
size_t			ft_wstrlen(const wchar_t *ws);
void			ft_wcstombs(wchar_t *s, char **s1,
					size_t *s1_len, int ws_len);
void			ft_printf_print_pav(t_pav *pav);

#endif
