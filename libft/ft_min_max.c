/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_min_max.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:26:45 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:26:47 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double			ft_min(double a, double b)
{
	return (a < b ? a : b);
}

double			ft_max(double a, double b)
{
	return (a > b ? a : b);
}

int				ft_between(double x, double min, double max)
{
	return (min <= x && x <= max);
}

double			ft_arr_min(double *arr, int count)
{
	int			i;
	double		result;

	i = 0;
	result = arr[0];
	while (++i < count)
		result = ft_min(result, arr[i]);
	return (result);
}

double			ft_arr_max(double *arr, int count)
{
	int			i;
	double		result;

	i = 0;
	result = arr[0];
	while (++i < count)
		result = ft_max(result, arr[i]);
	return (result);
}
