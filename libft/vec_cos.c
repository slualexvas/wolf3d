/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_cos.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 17:29:57 by oslutsky          #+#    #+#             */
/*   Updated: 2017/12/17 12:34:51 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double		vec_cos_sqr(t_vec a, t_vec b)
{
	return (pow(vec_mult_scal(a, b), 2) /
			(vec_mult_scal(a, a) * vec_mult_scal(b, b)));
}

double		vec_cos(t_vec a, t_vec b)
{
	double	len1;

	len1 = vec_mult_scal(a, a) * vec_mult_scal(b, b);
	if (len1 == 0)
		return (0);
	else
		return (vec_mult_scal(a, b) / sqrt(len1));
}

t_vec		vec_norm(t_vec v)
{
	return (vec_mult_num(v, 1 / sqrt(vec_mult_scal(v, v))));
}
