/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 13:04:32 by oslutsky          #+#    #+#             */
/*   Updated: 2017/11/12 09:14:35 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYS_H
# define KEYS_H

# ifdef __linux__

#  define KEY_ESC			65307
#  define KEY_LEFT			65361
#  define KEY_RIGHT			65363
#  define KEY_UP			65362
#  define KEY_DOWN			65364
#  define KEY_PLUS			65451
#  define KEY_PLUS2			65451
#  define KEY_MINUS			65453
#  define KEY_MINUS2		65453

#  define KEY_A				97
#  define KEY_S				115
#  define KEY_D				100
#  define KEY_W				119

# else

#  define KEY_ESC			53
#  define KEY_LEFT			123
#  define KEY_RIGHT			124
#  define KEY_UP			126
#  define KEY_DOWN			125
#  define KEY_PLUS			24
#  define KEY_PLUS2			69
#  define KEY_MINUS			27
#  define KEY_MINUS2		78

#  define KEY_A				0
#  define KEY_S				1
#  define KEY_D				2
#  define KEY_W				13

# endif
#endif
