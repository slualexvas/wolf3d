/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_linear_ops.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 14:04:21 by oslutsky          #+#    #+#             */
/*   Updated: 2017/09/16 13:58:02 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_vec			vec_lin_comb(double a1, t_vec v1, double a2, t_vec v2)
{
	t_vec		res;

	res.x = a1 * v1.x + a2 * v2.x;
	res.y = a1 * v1.y + a2 * v2.y;
	res.z = a1 * v1.z + a2 * v2.z;
	return (res);
}

t_vec			vec_plus(t_vec v1, t_vec v2)
{
	return (vec_lin_comb(1, v1, 1, v2));
}

t_vec			vec_minus(t_vec v1, t_vec v2)
{
	return (vec_lin_comb(1, v1, -1, v2));
}

t_vec			vec_mult_num(t_vec v, double num)
{
	return (vec_lin_comb(num, v, 0, v));
}
