/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 11:13:43 by oslutsky          #+#    #+#             */
/*   Updated: 2017/12/02 15:52:48 by ykondrat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# define BUFF_SIZE 128
# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdarg.h>
# include "rgb.h"
# include "keys.h"
# include "ft_printf/ft_printf.h"
# include <inttypes.h>
# include <math.h>

size_t			ft_strlen(const char *s);
void			*ft_memset(void *b, int c, size_t len);
void			ft_bzero(void *s, size_t n);
void			*ft_memcpy(void *dst, const void *src, size_t n);
void			*ft_memccpy(void *dst, const void *src, int c, size_t n);
void			*ft_memmove(void *dst, const void *src, size_t len);
void			*ft_memchr(const void *s, int c, size_t n);
int				ft_memcmp(const void *s1, const void *s2, size_t n);
char			*ft_strdup(const char *s1);
char			*ft_strcpy(char *dst, const char *src);
char			*ft_strncpy(char *dst, const char *src, size_t len);
char			*ft_strcat(char *s1, const char *s2);
char			*ft_strncat(char *s1, const char *s2, size_t n);
size_t			ft_strlcat(char *dst, const char *src, size_t size);
char			*ft_strchr(const char *s, int c);
char			*ft_strrchr(const char *s, int c);
char			*ft_strstr(const char *big, const char *little);
char			*ft_strnstr(const char *big, const char *little, size_t len);
int				ft_strcmp(const char *s1, const char *s2);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
int				ft_atoi(const char *s);
double			ft_atof(char *str);
int				ft_isalpha(int c);
int				ft_isupper(int c);
int				ft_islower(int c);
int				ft_isdigit(int c);
int				ft_isalnum(int c);
int				ft_isascii(int c);
int				ft_isprint(int c);
int				ft_toupper(int c);
int				ft_tolower(int c);
void			*ft_memalloc(size_t size);
void			ft_memdel(void **ap);
char			*ft_strnew(size_t size);
void			ft_strdel(char **as);
void			ft_strclr(char *s);
void			ft_striter(char *s, void (*f)(char *));
void			ft_striteri(char *s, void (*f)(unsigned int, char *));
char			*ft_strmap(char const *s, char (*f)(char));
char			*ft_strmapi(char const *s, char(*f)(unsigned int, char));
int				ft_strequ(char const *s1, char const *s2);
int				ft_strnequ(char const *s1, char const *s2, size_t n);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
char			*ft_strjoin(char const *s1, char const *s2);
char			*ft_strtrim(char const *s);
char			**ft_strsplit(char const *s, char c);
char			*ft_itoa(int n);
void			ft_putchar(char c);
void			ft_putstr(char const *s);
void			ft_putendl(char const *s);
void			ft_putnbr(int n);
void			ft_putchar_fd(char c, int fd);
void			ft_putstr_fd(char const *s, int fd);
void			ft_putendl_fd(char const *s, int fd);
void			ft_putnbr_fd(int n, int fd);

typedef struct	s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}				t_list;

t_list			*ft_lstnew(void const *content, size_t content_size);
void			ft_lstdelone(t_list **alst, void (*del)(void*, size_t));
void			ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void			ft_lstadd(t_list **alst, t_list *new);
void			ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list			*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));

void			ft_lstprint(t_list *lst);

typedef struct	s_buf
{
	char		buf[BUFF_SIZE + 1];
	size_t		cursor;
}				t_buf;

int				get_next_line(const int fd, char **line);

typedef struct	s_vec
{
	double		x;
	double		y;
	double		z;
}				t_vec;

t_vec			vec(double x, double y, double z);
t_vec			vec_plus(t_vec a, t_vec b);
t_vec			vec_minus(t_vec a, t_vec b);
t_vec			vec_mult_num(t_vec a, double num);
t_vec			vec_lin_comb(double a1, t_vec v1, double a2, t_vec v2);
double			vec_mult_scal(t_vec a, t_vec b);
double			vec_cos_sqr(t_vec a, t_vec b);
double			vec_cos(t_vec a, t_vec b);
t_vec			vec_norm(t_vec v);

void			split_res_free(char ***split_res);
int				ft_is_int(char *s);

char			*ft_uintmax_to_str_base(uintmax_t a, unsigned char base,
					int is_uppercase, size_t min_width);
char			*ft_intmax_to_str_base(intmax_t a, unsigned char base,
					int is_uppercase, size_t min_width);
char			*ft_longdouble_to_str_base(long double a, unsigned char base,
					int is_uppercase, unsigned char precision);
intmax_t		ft_i_pow_n(intmax_t i, unsigned char n);

typedef struct	s_quadratic_equation
{
	double		a;
	double		b;
	double		c;
	double		x1;
	double		x2;
	int			has_solutions;
}				t_quadratic_equation;

void			solve_quadratic_equation(t_quadratic_equation *eq);

double			ft_min(double a, double b);
double			ft_max(double a, double b);
int				ft_between(double x, double a, double b);
double			ft_arr_min(double *arr, int count);
double			ft_arr_max(double *arr, int count);

#endif
