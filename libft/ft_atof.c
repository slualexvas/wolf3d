/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ykondrat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/16 15:21:26 by ykondrat          #+#    #+#             */
/*   Updated: 2017/10/15 15:28:09 by ykondrat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double		ft_atof(char *str)
{
	double	res;
	double	res2;
	int		symbol;
	int		len;

	while ((*str >= 9 && *str <= 13) || *str == 32)
		str++;
	if (*str == 43 || *str == 45)
	{
		symbol = *str;
		str++;
	}
	res = (double)ft_atoi(str);
	while (*str && *str != '.')
		str++;
	*str == '.' ? str++ : 0;
	res2 = (double)ft_atoi(str);
	len = ft_strlen(str);
	while (len--)
		res2 /= 10;
	return (symbol == 45 ? -(res + res2) : (res + res2));
}
