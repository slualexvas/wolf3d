/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rgb.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/19 14:39:20 by oslutsky          #+#    #+#             */
/*   Updated: 2017/10/22 18:01:12 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RGB_H
# define RGB_H

typedef struct	s_rgb
{
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;
}				t_rgb;

int				rgb_to_int(t_rgb rgb);
t_rgb			ft_rgb(unsigned char r, unsigned char g, unsigned char b);
t_rgb			int_to_rgb(int color);
t_rgb			weighted_color(t_rgb color1, t_rgb color2, float w1, float w2);
t_rgb			color_by_light(t_rgb color, double light);
#endif
