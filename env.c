/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:21 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:04:22 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

void	env_init(t_env *env, int argc, char **argv)
{
	env->mlx.mlx = NULL;
	map_init(env, argc, argv);
	ft_mlx_init(&(env->mlx));
	player_init(env);
}

void	env_destroy(t_env *env)
{
	map_destroy(env);
	if (env->mlx.mlx != NULL)
		ft_mlx_destroy(&(env->mlx));
}
