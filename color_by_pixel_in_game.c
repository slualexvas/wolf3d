/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_by_pixel_in_game.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 19:59:59 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:00:42 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

double				texture_on_x_wall(t_vec pt)
{
	if (pt.z > 0.8)
		return (0.8);
	else if (pt.z > 0.6)
		return (1.5);
	else if (pt.z > 0.4)
		return (0.6);
	else if (pt.z > 0.2)
		return (1.5);
	else
		return (0.8);
}

double				texture_on_y_wall(t_vec pt)
{
	return (is_near_int(pt.x * 10) || is_near_int(pt.z * 5) * 10 ? 2 : 0.5);
}

t_rgb				color_on_ceil(void)
{
	return (ft_rgb(128, 128, 255));
}

t_rgb				color_on_floor(void)
{
	return (ft_rgb(128, 128, 128));
}

t_rgb				color_by_pixel_in_game(t_env *env, int x, int y)
{
	t_vec			pt;
	t_ray			ray;

	if (env->rays[x].len == INFTY)
		return (y > (HEIGHT_MAIN / 2) ? color_on_floor() : color_on_ceil());
	ray = env->rays[x];
	ray.direction.z = 0.5 - (double)y / HEIGHT_MAIN;
	pt = vec_lin_comb(1, ray.pt, ray.len, ray.direction);
	if (pt.z >= 1)
		return (color_on_ceil());
	else if (pt.z <= 0)
		return (color_on_floor());
	else if (is_near_int(pt.x) && is_near_int(pt.y))
		return (ft_rgb(200, 200, 0));
	else if (is_near_int(pt.x))
		return (env->rays[x].direction.x > 0 ?
			color_on_wall_x_min(pt) : color_on_wall_x_max(pt));
	else
		return (env->rays[x].direction.y > 0 ?
			color_on_wall_y_min(pt) : color_on_wall_y_max(pt));
}
