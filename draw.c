/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 21:44:04 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/23 21:44:07 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

void				recalc_rays(t_env *env)
{
	int				x;
	double			deg_min;
	double			deg_max;

	deg_min = deg_to_0_360(env->player.deg - ANGLE_SIGHT);
	deg_max = deg_to_0_360(env->player.deg + ANGLE_SIGHT);
	env->rays[0].direction = vec(
			-cos(MATH_PI * deg_min / 180),
			-sin(MATH_PI * deg_min / 180), 0);
	env->rays[WIDTH - 1].direction = vec(
			-cos(MATH_PI * deg_max / 180),
			-sin(MATH_PI * deg_max / 180), 0);
	x = 1;
	while (x < WIDTH - 1)
	{
		env->rays[x].direction = vec_lin_comb(
				1 - ((double)x) / WIDTH, env->rays[0].direction,
				((double)x) / WIDTH, env->rays[WIDTH - 1].direction);
		env->rays[x].pt = vec(env->player.x, env->player.y, 0.5);
		intersect_ray_with_blocks(&(env->rays[x]), env);
		x++;
	}
}

void				ft_put_pixel(t_env *env, int x, int y, t_rgb color)
{
	if (x < 0 || y < 0 || x >= WIDTH || y >= HEIGHT)
		return ;
	env->mlx.img_data_addr[y * env->mlx.szline + x * 4 + 2] = color.r;
	env->mlx.img_data_addr[y * env->mlx.szline + x * 4 + 1] = color.g;
	env->mlx.img_data_addr[y * env->mlx.szline + x * 4 + 0] = color.b;
	env->mlx.img_data_addr[y * env->mlx.szline + x * 4 + 3] = 0;
}

void				do_draw(t_env *env)
{
	int				x;
	int				y;
	t_rgb			color;

	recalc_rays(env);
	y = -1;
	while (++y < HEIGHT)
	{
		x = -1;
		while (++x < WIDTH)
		{
			color = (y >= HEIGHT_MAIN)
					? color_by_pixel_in_map(env, x, y - HEIGHT_MAIN)
					: color_by_pixel_in_game(env, x, y);
			ft_put_pixel(env, x, y, color);
		}
	}
}

void				draw(t_env *env)
{
	env->mlx.img = mlx_new_image(env->mlx.mlx, WIDTH, HEIGHT);
	env->mlx.img_data_addr = mlx_get_data_addr(
		env->mlx.img,
		&(env->mlx.bpp),
		&(env->mlx.szline),
		&(env->mlx.endian));
	do_draw(env);
	mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->mlx.img, 0, 0);
	env->mlx.img_data_addr = NULL;
}
