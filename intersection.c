/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersection.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:30 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:04:25 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

double				intersect_ray_with_segment_by_const_x(
						t_ray *ray, double x, double ymin, double ymax)
{
	double	t;

	if (ray->direction.x == 0)
		return (INFTY);
	t = (x - ray->pt.x) / ray->direction.x;
	return (
		(t >= 0 && ft_between(ray->pt.y + t * ray->direction.y, ymin, ymax)) ?
			t : INFTY);
}

double				intersect_ray_with_segment_by_const_y(
						t_ray *ray, double y, double xmin, double xmax)
{
	double	t;

	if (ray->direction.y == 0)
		return (INFTY);
	t = (y - ray->pt.y) / ray->direction.y;
	return (
		(t >= 0 && ft_between(ray->pt.x + t * ray->direction.x, xmin, xmax))
			? t : INFTY);
}

double				intersect_ray_with_one_block(int x, int y, t_ray *ray)
{
	double	lengths[4];

	lengths[0] = intersect_ray_with_segment_by_const_x(ray, x, y, y + 1);
	lengths[1] = intersect_ray_with_segment_by_const_x(ray, x + 1, y, y + 1);
	lengths[2] = intersect_ray_with_segment_by_const_y(ray, y, x, x + 1);
	lengths[3] = intersect_ray_with_segment_by_const_y(ray, y + 1, x, x + 1);
	return (ft_arr_min(lengths, 4));
}

void				intersect_ray_with_blocks(t_ray *ray, t_env *env)
{
	int				y;
	int				x;

	ray->len = INFTY;
	y = 0;
	while (y < env->map.height)
	{
		x = 0;
		while (x < env->map.with)
		{
			if (env->map.map[y][x] == BLOCK)
				ray->len = ft_min(ray->len,
						intersect_ray_with_one_block(x, y, ray));
			x++;
		}
		y++;
	}
}
