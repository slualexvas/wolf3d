/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors_on_walls.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:13 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:03:54 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

t_rgb				color_on_wall_x_min(t_vec pt)
{
	return (color_by_light(ft_rgb(255, 128, 0), texture_on_x_wall(pt)));
}

t_rgb				color_on_wall_x_max(t_vec pt)
{
	return (color_by_light(ft_rgb(128, 255, 0), texture_on_x_wall(pt)));
}

t_rgb				color_on_wall_y_min(t_vec pt)
{
	return (color_by_light(ft_rgb(0, 255, 128), texture_on_y_wall(pt)));
}

t_rgb				color_on_wall_y_max(t_vec pt)
{
	return (color_by_light(ft_rgb(0, 128, 255), texture_on_y_wall(pt)));
}
