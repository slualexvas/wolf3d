/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   include.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:17:50 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:17:52 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INCLUDE_H
# define INCLUDE_H

# define WIDTH 			1200
# define HEIGHT_MAIN	800
# define HEIGHT 		1200
# define ANGLE_SIGHT	30
# define INFTY			1000000
# define MAX_MAP_SIZE	20

# define MATH_PI		3.1415926

# define SPACE	'.'
# define BLOCK	'B'

# include <mlx.h>
# include <fcntl.h>
# include <math.h>
# include "libft/libft.h"

typedef struct		s_mlx
{
	void			*mlx;
	void			*win;
	void			*img;
	int				bpp;
	int				szline;
	int				endian;
	char			*img_data_addr;
}					t_mlx;

typedef struct		s_map
{
	char			**map;
	int				with;
	int				height;
}					t_map;

typedef struct		s_player
{
	float			x;
	float			y;
	int				deg;
}					t_player;

typedef struct		s_ray
{
	t_vec			pt;
	double			len;

	t_vec			direction;
}					t_ray;

typedef struct		s_env
{
	t_mlx			mlx;
	t_map			map;
	t_player		player;
	t_ray			rays[WIDTH];
}					t_env;

void				draw(t_env *env);

void				env_init(t_env *env, int argc, char **argv);
void				env_destroy(t_env *env);

void				ft_mlx_init(t_mlx *mlx);
void				ft_mlx_destroy(t_mlx *mlx);

void				map_init(t_env *env, int argc, char **argv);
void				map_destroy(t_env *env);
void				map_x_y_init(t_env *env, char **argv);
void				map_body_init(t_env *env, char **argv);
void				map_body_validate(t_env *env);

int					key_hook(int keycode, void *param);
int					mouse_hook(int button, int x, int y, void *param);
int					hook_cross(void *w);
void				key_hook_arrows(int keycode, t_env *env);

void				print_error_and_exit(char *msg, t_env *env);

t_rgb				color_by_pixel(t_env *env, int x, int y);
t_rgb				color_by_pixel_in_map(t_env *env, int x, int y);
t_rgb				color_by_pixel_in_game(t_env *env, int x, int y);

t_rgb				color_on_wall_x_min(t_vec pt);
t_rgb				color_on_wall_x_max(t_vec pt);
t_rgb				color_on_wall_y_min(t_vec pt);
t_rgb				color_on_wall_y_max(t_vec pt);

double				texture_on_x_wall(t_vec pt);
double				texture_on_y_wall(t_vec pt);

void				player_init(t_env *env);
void				player_move(t_env *env, double x_new, double y_new);

double				deg_to_0_360(double deg);
void				intersect_ray_with_blocks(t_ray *ray, t_env *env);

int					is_near_int(double x);
#endif
