/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 22:20:47 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 22:20:51 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

void		map_init(t_env *env, int argc, char **argv)
{
	int		fd;

	env->map.map = NULL;
	if (argc <= 1 || argv[1] == NULL)
		print_error_and_exit("argc == 1 || argv[1] == NULL", env);
	fd = open(argv[1], O_RDONLY);
	if (fd < 0)
		print_error_and_exit("map_init: fd < 0", env);
	close(fd);
	map_x_y_init(env, argv);
	if (env->map.with == 0 || env->map.height == 0)
		print_error_and_exit("Empty map", env);
	if (env->map.with > MAX_MAP_SIZE || env->map.height > MAX_MAP_SIZE)
		print_error_and_exit("map size too big", env);
	map_body_init(env, argv);
	map_body_validate(env);
}

void		map_destroy(t_env *env)
{
	split_res_free(&(env->map.map));
}

void		map_x_y_init(t_env *env, char **argv)
{
	int		fd;
	char	*line;
	int		space_exists;

	env->map.height = 0;
	space_exists = 0;
	fd = open(argv[1], O_RDONLY);
	while (get_next_line(fd, &line) == 1)
	{
		if (env->map.height == 0)
			env->map.with = (int)ft_strlen(line);
		else if ((int)ft_strlen(line) != env->map.with)
			break ;
		env->map.height++;
		ft_strdel(&line);
	}
	close(fd);
	if (line != NULL)
	{
		ft_strdel(&line);
		print_error_and_exit("map_init: non-equal length of lines", env);
	}
}

void		map_body_init(t_env *env, char **argv)
{
	int		y;
	int		fd;
	char	*line;

	env->map.map = ft_memalloc((env->map.height + 1) * sizeof(char*));
	y = 0;
	fd = open(argv[1], O_RDONLY);
	while (get_next_line(fd, &line) == 1)
	{
		env->map.map[y] = ft_strdup(line);
		ft_strdel(&line);
		y++;
	}
	close(fd);
	env->map.map[y] = NULL;
}

void		map_body_validate(t_env *env)
{
	int		y;
	int		x;
	int		only_blocks;

	only_blocks = 1;
	y = 0;
	while (y < env->map.height)
	{
		x = 0;
		while (x < env->map.with)
		{
			if (env->map.map[y][x] != BLOCK && env->map.map[y][x] != SPACE)
				print_error_and_exit("incorrect character", env);
			if (env->map.map[y][x] == SPACE)
				only_blocks = 0;
			x++;
		}
		y++;
	}
	if (only_blocks == 1)
		print_error_and_exit("the map has only blocks", env);
}
