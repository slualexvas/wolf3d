/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_hook.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:54 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:04:13 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

int			mouse_hook(int button, int x, int y, void *param)
{
	t_env	*env;

	if (x < 0 || y < 0 || x > WIDTH || y > HEIGHT || button < 0 ||
		param == NULL)
		return (0);
	else
	{
		env = (t_env*)param;
		draw(env);
		return (0);
	}
}
