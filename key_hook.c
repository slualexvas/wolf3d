/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:37 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:04:27 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

void		key_hook_arrows(int keycode, t_env *env)
{
	if (keycode == KEY_LEFT || keycode == KEY_RIGHT
		|| keycode == KEY_UP || keycode == KEY_DOWN)
	{
		if (keycode == KEY_RIGHT)
			env->player.deg = deg_to_0_360(env->player.deg + 15);
		if (keycode == KEY_LEFT)
			env->player.deg = deg_to_0_360(env->player.deg - 15);
		if (keycode == KEY_UP)
			player_move(env,
				env->player.x - 0.3 * cos(env->player.deg * MATH_PI / 180),
				env->player.y - 0.3 * sin(env->player.deg * MATH_PI / 180));
		if (keycode == KEY_DOWN)
			player_move(env,
				env->player.x + 0.3 * cos(env->player.deg * MATH_PI / 180),
				env->player.y + 0.3 * sin(env->player.deg * MATH_PI / 180));
		draw(env);
	}
}

void		key_hook_wasd(int keycode, t_env *env)
{
	if (keycode == KEY_W || keycode == KEY_A
		|| keycode == KEY_S || keycode == KEY_D)
	{
		if (keycode == KEY_A)
			player_move(env,
				env->player.x - 0.3 * sin(env->player.deg * MATH_PI / 180),
				env->player.y + 0.3 * cos(env->player.deg * MATH_PI / 180));
		if (keycode == KEY_D)
			player_move(env,
				env->player.x + 0.3 * sin(env->player.deg * MATH_PI / 180),
				env->player.y - 0.3 * cos(env->player.deg * MATH_PI / 180));
		if (keycode == KEY_W)
			player_move(env,
				env->player.x - 0.3 * cos(env->player.deg * MATH_PI / 180),
				env->player.y - 0.3 * sin(env->player.deg * MATH_PI / 180));
		if (keycode == KEY_S)
			player_move(env,
				env->player.x + 0.3 * cos(env->player.deg * MATH_PI / 180),
				env->player.y + 0.3 * sin(env->player.deg * MATH_PI / 180));
		draw(env);
	}
}

int			key_hook(int keycode, void *param)
{
	t_env	*env;

	env = (t_env*)param;
	key_hook_arrows(keycode, env);
	key_hook_wasd(keycode, env);
	if (keycode == KEY_ESC)
		print_error_and_exit("window closed by ESC", env);
	return (0);
}

int			hook_cross(void *w)
{
	print_error_and_exit("window closed by KRESTIK", (t_env*)w);
	return (0);
}

double		deg_to_0_360(double deg)
{
	while (deg >= 360)
		deg -= 360;
	while (deg < 0)
		deg += 360;
	return (deg);
}
