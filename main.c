/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:42 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:04:29 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

int	main(int argc, char **argv)
{
	t_env		env;

	env_init(&env, argc, argv);
	mlx_hook(env.mlx.win, 2, 5, key_hook, &env);
	mlx_hook(env.mlx.win, 17, 1L << 17, hook_cross, &env);
	mlx_mouse_hook(env.mlx.win, mouse_hook, &env);
	draw(&env);
	mlx_loop(env.mlx.mlx);
	return (0);
}
