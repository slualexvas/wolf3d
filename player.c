/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:58 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:04:08 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

void		player_init(t_env *env)
{
	int		x;
	int		y;

	env->player.deg = 180;
	y = 0;
	while (y < env->map.height)
	{
		x = 0;
		while (x < env->map.with)
		{
			if (env->map.map[y][x] == SPACE)
			{
				env->player.x = x + 0.5;
				env->player.y = y + 0.5;
				return ;
			}
			x++;
		}
		y++;
	}
}

void		player_move(t_env *env, double x_new, double y_new)
{
	if (x_new >= 0 && x_new < env->map.with
			&& env->map.map[(int)env->player.y][(int)x_new] == SPACE)
		env->player.x = x_new;
	if (y_new >= 0 && y_new < env->map.height
			&& env->map.map[(int)y_new][(int)env->player.x] == SPACE)
		env->player.y = y_new;
}
