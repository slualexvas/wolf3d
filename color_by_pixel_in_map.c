/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_by_pixel_in_map.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:04 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:03:41 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

int					deg(double x, double y)
{
	int				deg_atan;

	if (x == 0)
		return (y > 0 ? 90 : 270);
	deg_atan = atan(y / x) * 180 / MATH_PI;
	if (y > 0)
		return (x > 0 ? deg_atan : deg_atan + 180);
	else
		return (x > 0 ? deg_atan + 360 : deg_atan + 180);
}

t_rgb				color_by_pixel_in_map(t_env *env, int x, int y)
{
	double			x_cell;
	double			y_cell;
	double			x_vec;
	double			y_vec;

	x_cell = (double)x * env->map.with / WIDTH;
	y_cell = (double)y * env->map.height / (HEIGHT - HEIGHT_MAIN);
	x_vec = env->player.x - x_cell;
	y_vec = env->player.y - y_cell;
	if (env->map.map[(int)y_cell][(int)x_cell] == BLOCK)
		return (is_near_int(x_cell) || is_near_int(y_cell) ?
			ft_rgb(128, 0, 0) : ft_rgb(255, 0, 0));
	else if (abs(deg(x_vec, y_vec) - env->player.deg) < ANGLE_SIGHT)
		return (ft_rgb(255, 255, 255));
	else if (pow(x_vec, 2) + pow(y_vec, 2) < 0.1)
		return (ft_rgb(255, 255, 0));
	else
		return (ft_rgb(0, 0, 0));
}
