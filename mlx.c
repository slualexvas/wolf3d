/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oslutsky <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 20:02:48 by oslutsky          #+#    #+#             */
/*   Updated: 2018/10/12 20:04:31 by oslutsky         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include.h"

void	ft_mlx_init(t_mlx *mlx)
{
	mlx->mlx = mlx_init();
	mlx->win = mlx_new_window(mlx->mlx, WIDTH, HEIGHT, "w");
	mlx->img = NULL;
	mlx->img_data_addr = NULL;
	mlx->szline = 0;
	mlx->bpp = 0;
	mlx->endian = 0;
}

void	ft_mlx_destroy(t_mlx *mlx)
{
	mlx_destroy_window(mlx->mlx, mlx->win);
}
